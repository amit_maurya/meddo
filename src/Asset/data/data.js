export default [
  {
    id: 1,
    name: 'Health',
    title: 'It is a long established fact that a reader will distracted by readabl',
    description: 'St. John’s Hospital is a Registered Charity under the Charities Acts (Registered Charity No. 20000394) and is administered and managed in accordance with a Hospital Constitution approved by the Charities Regulatory Authority.  The current Hospital Constitution was approved in 2018.  The property is vested in Trustees.',
    date: '6 June',
    color: 'antiquewhite'
  },
  {
    id: 2,
    name: 'Get a head...',
    title: 'There are many variations of passages',
    description: 'St. John’s Hospital is a Registered Charity under the Charities Acts (Registered Charity No. 20000394) and is administered and managed in accordance with a Hospital Constitution approved by the Charities Regulatory Authority.  The current Hospital Constitution was approved in 2018.  The property is vested in Trustees.',
    date: '5 June',
    color: 'aliceblue'
  },
  {
    id: 3,
    name: 'Shop List',
    listItem: [{ id: 1, item: 'Clothes' }, { id: 2, item: 'Table' },
    { id: 1, item: 'Pizza' }, { id: 1, item: 'Book' },
    { id: 1, item: 'Pen' }, { id: 1, item: 'Keyboard' }],
    title: null,
    description: 'St. John’s Hospital is a Registered Charity under the Charities Acts (Registered Charity No. 20000394) and is administered and managed in accordance with a Hospital Constitution approved by the Charities Regulatory Authority.  The current Hospital Constitution was approved in 2018.  The property is vested in Trustees.',
    date: '7 June',
    color: 'beige'
  },
  {
    id: 4,
    name: 'Family',
    title: 'There are many variations of passages',
    description: 'St. John’s Hospital is a Registered Charity under the Charities Acts (Registered Charity No. 20000394) and is administered and managed in accordance with a Hospital Constitution approved by the Charities Regulatory Authority.  The current Hospital Constitution was approved in 2018.  The property is vested in Trustees.',
    date: '6 June',
    color: 'cadetblue'
  },
  {
    id: 5,
    name: 'Food',
    title: 'There are many variations of passages',
    description: 'St. John’s Hospital is a Registered Charity under the Charities Acts (Registered Charity No. 20000394) and is administered and managed in accordance with a Hospital Constitution approved by the Charities Regulatory Authority.  The current Hospital Constitution was approved in 2018.  The property is vested in Trustees.',
    date: '6 June',
    color: 'whitesmoke'
  },
  {
    id: 6,
    name: 'Personal',
    title: 'There are many variations of passages',
    description: 'St. John’s Hospital is a Registered Charity under the Charities Acts (Registered Charity No. 20000394) and is administered and managed in accordance with a Hospital Constitution approved by the Charities Regulatory Authority.  The current Hospital Constitution was approved in 2018.  The property is vested in Trustees.',
    date: '8 June',
    color: 'cornsilk'
  },
]